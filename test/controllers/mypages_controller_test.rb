require 'test_helper'

class MypagesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get mypages_index_url
    assert_response :success
  end

  test "should get msg" do
    get mypages_msg_url
    assert_response :success
  end

  test "should get msgCreate" do
    get mypages_msgCreate_url
    assert_response :success
  end

end
