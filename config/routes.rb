Rails.application.routes.draw do

  get 'mypages/index'


  get 'mypages/messages' => 'messages#index'
  get 'mypages/create/message' => "messages#new"
  post 'mypages/create/message' => "messages#create"

  root "sessions#new"

  controller :sessions do
    get "login" => :new
    post "login" => :create
    get "logout" => :destroy
  end


end
