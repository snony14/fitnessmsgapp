class Message < ApplicationRecord
  validates :subject, presence: true, length: {maximum: 30}
  validates :body , presence: true

  belongs_to :sender, :foreign_key => :sender_id, class_name: 'Client'
  belongs_to :receiver, :foreign_key => :receiver_id, class_name: 'Client'
end
