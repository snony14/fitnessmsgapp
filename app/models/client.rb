class Client < ApplicationRecord

  has_many :users, :class_name => "Client"
  belongs_to :trainer, :class_name => "Client",
              :foreign_key => "trainer_id"
  
  has_many :sent_messages, :class_name=> 'Message', :foreign_key=>'sender_id', :dependent=>:nullify
  has_many :recieved_messages, :class_name=> 'Message', :foreign_key=>'receiver_id', :dependent=>:nullify

  has_secure_password
end
