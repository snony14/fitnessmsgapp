class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

    before_action :client_in_session

  protected

  def client_in_session
    @client = Client.find_by_id(session[:client_id])
    if @client.nil?
      redirect_to login_url
    end
  end



end
