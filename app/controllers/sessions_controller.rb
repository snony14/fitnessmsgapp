class SessionsController < ApplicationController
  skip_before_action :client_in_session, only: [:new, :create]


  def new
  end

  def create
    client = Client.find_by(username:params[:session][:username])
    if client && client.authenticate(params[:session][:password])
      session[:client_id] = client.id
      redirect_to mypages_index_path
    else
      flash[:notice] = "Invalid username and password combination"
      redirect_to login_url
    end
  end

  def destroy
    session[:client_id] = nil
    redirect_to login_path
  end
end
