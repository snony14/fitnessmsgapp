class MessagesController < ApplicationController

  def index
    @messages = @client.sent_messages + @client.recieved_messages
  end

  def new
    trainer = [@client.trainer]
    users = Client.where('trainer_id' => @client.id)
    @users = trainer + users
  end

  def create
    sender = @client
    receiver = Client.find_by_id(params[:msg][:user_id])
    subject = params[:msg][:subject]
    content = params[:msg][:body]
    message = Message.create(subject:subject, body:content, sender:@client, receiver:receiver)
    if message.save
      redirect_to action: 'index'
    else
      redirect_to action: 'new'
    end
  end
end
