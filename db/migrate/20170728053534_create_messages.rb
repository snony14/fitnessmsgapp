class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages, :force => true do |t|
      t.integer  "sender_id"
      t.integer  "receiver_id"
      t.string   "subject"
      t.string   "body"

      t.timestamps
    end
  end
end
