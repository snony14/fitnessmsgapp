class AddTrainerToClients < ActiveRecord::Migration[5.0]
  def change
    add_reference :clients, :trainer, foreign_key: true, index:true
    add_column :clients, :admin, :boolean, :default =>false
  end
end
